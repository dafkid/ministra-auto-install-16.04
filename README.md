## Whats Ministra.
For those of you that don't know what Ministra is. Ministra (Formaly Stalker Middleware) is a self hosted semi-open source IPTV streaming platform that's used by thousands of people around the world, the Minstara software itself is centred more around IPTV providers than customers but allows a provider to host a central administration panel to distribute Live TV content, on Demand video content including TV Shows & Movies. If you ever thought about hosting your own Movie/TV Streaming service, Ministra is where you'd need to start!
## Semi-Open Source?
Yeah I think it's odd too! Ministra is considered Open Source software but you have to officially submit a request to download the required installation files, this isn't outside the norm for a company like this, they are collecting information on clients to increase there sales reach in other areas of their business. Safe to say following this guide your not going to need to fill in any form listing your name and address or any other personal information.
## The Why.
A close friend contacted me a couple of days ago and pointed me in the direction of this script to auto install Ministra, I ran my eyes over it and delved into what it was actually doing and to be fair it was OK! it followed the same installation instructions as the MInistra guides online but having spent some time messing with Ministra over the years I know that Ministra's guide is not only hard for follow but it's also flawed in some areas too, hence how I ended up here.
## The Script.
The script itself is no different to any other script you would expect to find on this site, it follows the basic installation listed by Ministra but with a couple of subtle differences. 
* The script will download the required installation files from my own sources without the need to disclose personal information to third parties. 
* Ministra recommend that you should reverse proxy from Nginx to Apache and have Apache running on port 88. I could understand this if Nginx was actually doing something crucial but looking at their guide it's doing nothing at all, so this script does away with Nginx and sets everything up running on Apache with the interface available on port 80. 
* When following the Ministra guide, extremely week passwords are use for the Ministra software to access MySQL. Their default credentials are **Username: stalker_portal** and **Password: 1** and yes I have gained access to peoples stalker panels in the past using those credentials. This script will generate a 14 digit strong password and change the configuration files for you. 
### Get the Script.
```
cd ~
git clone https://gitlab.com/Sutherland/ministra-auto-install-16.04.git
```
### Run the Script.
```
cd ~/ministra-auto-install-16.04
sudo bash ./auto-ministra-16.04.sh
```
During the installation you will be asked to specify the password you would like set for the root MySQL user and later asked to enter it again when Ministra is configured. 

If you read this and you would like to give any feedback or discuss the script any further, please email sutherland@scripting.online.